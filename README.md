# Space Invaders

Space Invaders est un jeu simple en 2D développé en C++ avec l'utilisation de la bibliothèque SFML. Dans ce jeu, le joueur contrôle un vaisseau spatial pour défendre la Terre contre une invasion extraterrestre. Le but est de détruire le plus d'envahisseurs avant qu'ils n'atteignent la surface de la Terre.

Fonctionnalités
---------------

*   Contrôle du vaisseau spatial avec les touches directionnelles gauche et droite
*   Tir de projectiles avec ESPACE pour détruire les envahisseurs
*   Utilisation d'un bouclier avec S pour se protéger des attaques ennemies
*   Menu de démarrage et de pause pour gérer le jeu avec ECHAP

Dépendances
-----------

Ce projet utilise la bibliothèque SFML (Simple and Fast Multimedia Library) pour la gestion des graphismes et des événements. Assurez-vous d'installer SFML avant de compiler et d'exécuter le jeu.

Compilation et exécution
------------------------

1.  Assurez-vous d'avoir installé SFML sur votre système.
2.  Clonez ce dépôt sur votre machine.
3.  Compilez le jeu à l'aide de votre système de build préféré (par exemple, make ou Visual Studio).

Auteurs
-------

Ce jeu a été développé par moi même dans le cadre d'un projet personnel ou éducatif.