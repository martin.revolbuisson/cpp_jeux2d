#include "Projectile.h"

// Méthode pour mettre à jour la position des projectiles et les supprimer s'ils sortent de l'écran
void Projectile::update(sf::RenderWindow& window, float deltaTime) {
    // Mise à jour des balles
    for (auto it = bullets.begin(); it != bullets.end(); ) {
        float moveDistance = -50.0f; // Ajustement de la vitesse de déplacement
        it->move(0, moveDistance * deltaTime);

        // Suppression des balles qui sortent de l'écran
        if (it->getPosition().y + it->getSize().y < 0 || it->getPosition().y > window.getSize().y) {
            it = bullets.erase(it);
        }
        else {
            ++it;
        }
    }

    // Mise à jour des lasers
    for (auto it = lazers.begin(); it != lazers.end(); ) {
        float moveDistance = 40.0f; // Ajustement de la vitesse de déplacement
        it->move(0, moveDistance * deltaTime);

        // Suppression des lasers qui sortent de l'écran
        if (it->getPosition().y + it->getSize().y < 0 || it->getPosition().y > window.getSize().y) {
            it = lazers.erase(it);
        }
        else {
            ++it;
        }
    }
}

// Méthode pour dessiner les projectiles sur la fenêtre
void Projectile::draw(sf::RenderWindow& window) const {
    // Dessin des balles
    for (const auto& bullet : bullets) {
        window.draw(bullet);
    }
    // Dessin des lasers
    for (const auto& lazer : lazers) {
        window.draw(lazer);
    }
}

// Méthode renvoyant une référence à la liste des balles
std::vector<sf::RectangleShape>& Projectile::getBullets() {
    return bullets;
}

// Méthode renvoyant une référence à la liste des lasers
std::vector<sf::RectangleShape>& Projectile::getLazers() {
    return lazers;
}

// Méthode pour créer une nouvelle balle et l'ajouter à la liste des balles
void Projectile::shoot(sf::Vector2f entityPosition) {
    sf::RectangleShape projectile(sf::Vector2f(5, 20)); // Création d'une forme rectangulaire pour représenter une balle
    projectile.setFillColor(sf::Color::Red); // Couleur de la balle
    // Positionnement de la balle près de l'entité qui tire
    projectile.setPosition(entityPosition.x + projectile.getSize().x / 2 + 16, entityPosition.y - projectile.getSize().y);
    bullets.push_back(projectile); // Ajout de la balle à la liste des balles
}

// Méthode pour créer un nouveau laser et l'ajouter à la liste des lasers
void Projectile::fire(sf::Vector2f entityPosition) {
    sf::RectangleShape projectile(sf::Vector2f(5, 20)); // Création d'une forme rectangulaire pour représenter un laser
    projectile.setFillColor(sf::Color::Cyan); // Couleur du laser
    // Positionnement du laser près de l'entité qui tire
    projectile.setPosition(entityPosition.x + projectile.getSize().x * 6 - 6, entityPosition.y + projectile.getSize().y + 15);
    lazers.push_back(projectile); // Ajout du laser à la liste des lasers
}
