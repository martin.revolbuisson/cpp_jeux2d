#include "Pause.h"
#include <iostream>

// Constructeur initialisant les membres et chargeant la police de caractères
Pause::Pause(sf::RenderWindow& window)
    : m_window(window), m_resumed(false), m_quit(false) {
    // Chargement de la police de caractères
    if (!m_font.loadFromFile("./ui/Faster Stroker.otf")) {
        std::cerr << "Erreur : Impossible de charger la police de caractères." << std::endl;
        return;
    }

    // Configuration du titre du menu de pause
    m_titleText.setFont(m_font);
    m_titleText.setString("Pause Menu");
    m_titleText.setCharacterSize(48);
    m_titleText.setFillColor(sf::Color::White);
    m_titleText.setStyle(sf::Text::Bold);
    m_titleText.setPosition(300.f, 100.f);

    // Configuration du bouton de reprise
    m_resumeButton.setFont(m_font);
    m_resumeButton.setString("Resume");
    m_resumeButton.setCharacterSize(24);
    m_resumeButton.setFillColor(sf::Color::White);
    m_resumeButton.setPosition(400.f, 300.f);

    // Configuration du bouton de quitter
    m_quitButton.setFont(m_font);
    m_quitButton.setString("Quit");
    m_quitButton.setCharacterSize(24);
    m_quitButton.setFillColor(sf::Color::White);
    m_quitButton.setPosition(450.f, 400.f);
}

// Méthode pour gérer les entrées de l'utilisateur dans le menu de pause
void Pause::handleInput() {
    sf::Event event;
    while (m_window.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_window.close(); // Fermer la fenêtre si l'utilisateur clique sur la croix
        }
        else if (event.type == sf::Event::MouseButtonPressed) {
            if (event.mouseButton.button == sf::Mouse::Left) {
                sf::Vector2i mousePos = sf::Mouse::getPosition(m_window);

                // Vérifier si le bouton de reprise a été cliqué
                if (m_resumeButton.getGlobalBounds().contains(mousePos.x, mousePos.y)) {
                    m_resumed = true; // Indiquer que le jeu a été repris
                }
                // Vérifier si le bouton de quitter a été cliqué
                else if (m_quitButton.getGlobalBounds().contains(mousePos.x, mousePos.y)) {
                    m_quit = true; // Indiquer que le jeu a été quitté
                }
            }
        }
    }
}

// Méthode pour mettre à jour le menu de pause
void Pause::update() {
    // Aucune mise à jour nécessaire pour le menu de pause dans cette version simple
}

// Méthode pour dessiner le menu de pause sur la fenêtre
void Pause::draw() {
    m_window.clear(sf::Color::Black); // Effacer la fenêtre avec un fond noir

    // Dessiner le titre et les boutons sur la fenêtre
    m_window.draw(m_titleText);
    m_window.draw(m_resumeButton);
    m_window.draw(m_quitButton);

    m_window.display(); // Afficher le contenu dessiné sur la fenêtre
}

// Méthode pour vérifier si le jeu a été repris depuis le menu de pause
bool Pause::isResumed() const {
    return m_resumed;
}

// Méthode pour vérifier si le jeu a été quitté depuis le menu de pause
bool Pause::isQuit() const {
    return m_quit;
}
