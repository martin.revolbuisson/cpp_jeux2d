#include "Player.h"
#include <iostream>

// Constructeur initialisant les membres et chargeant la texture du joueur
Player::Player(sf::RenderWindow& window, Projectile& projectiles, Shield& shield) : window(window), projectiles(projectiles), shield(shield) {
    // Chargement de la texture du joueur depuis un fichier
    if (!texture.loadFromFile("./images/player.png")) {
        std::cout << "Erreur : Impossible de charger l'image du joueur." << std::endl;
        window.close(); // Fermer la fenêtre en cas d'échec de chargement de la texture
    }

    // Configuration de l'apparence et de la position initiale du joueur
    player.setTexture(texture);
    float scaleFactor = 0.1;
    player.setScale(scaleFactor, scaleFactor);
    player.setPosition(window.getSize().x / 2 - player.getGlobalBounds().width / 2, window.getSize().y - player.getGlobalBounds().height - 10);

    // Initialisation des horloges pour le contrôle du temps
    updateClock.restart();
    shieldClock.restart();
    reload_time = 0;
}

// Méthode pour gérer les entrées de l'utilisateur pour déplacer le joueur et activer le bouclier
void Player::handleInput() {
    // Déplacement du joueur vers la gauche
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && player.getPosition().x > 0) {
        player.move(-2, 0);
    }
    // Déplacement du joueur vers la droite
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && player.getPosition().x < window.getSize().x - player.getGlobalBounds().width) {
        player.move(2, 0);
    }
    // Activation du bouclier du joueur
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
        protect();
    }
    // Tir du joueur si le temps de recharge est écoulé
    if (reload_time <= 0) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            shoot();
            reload_time = 2; // Temps de recharge en secondes
        }
    }
}

// Méthode pour dessiner le joueur et ses projectiles sur la fenêtre
void Player::draw() {
    window.draw(player);
    projectiles.draw(window);
}

// Méthode pour mettre à jour l'état du joueur et de ses projectiles
void Player::update(float deltaTime) {
    sf::Time elapsed = updateClock.restart(); // Temps écoulé depuis la dernière mise à jour
    sf::Time timerShield = shieldClock.restart(); // Temps écoulé pour le bouclier
    reload_time -= elapsed.asSeconds(); // Mettre à jour le temps de recharge du tir
    reload_time -= timerShield.asSeconds(); // Mettre à jour le temps de recharge du bouclier
    projectiles.update(window, deltaTime); // Mettre à jour les projectiles du joueur
}

// Méthode pour faire tirer le joueur
void Player::shoot() {
    projectiles.shoot(player.getPosition()); // Faire tirer les projectiles depuis la position du joueur
}

// Méthode pour activer le bouclier du joueur
void Player::protect() {
    shield.protectPlayer(player.getPosition()); // Activer le bouclier du joueur à sa position actuelle
}
