#include "GameManager.h"
#include <iostream>
#include <SFML/Graphics.hpp>

GameManager::GameManager(sf::RenderWindow& window)
    : window(window), player(window, projectiles, shield), counterPoints(0), m_pauseMenu(m_pauseMenu) {
    counterPoints = 0;
    initScoreText();
    initShieldCharge();
    spawnInvaders();
    invasionInterval = 10.0f;
    pause = false;
    // Initialisation du menu de pause avec la fenêtre SFML
    Pause m_pauseMenu(window);
}

GameManager::~GameManager() {}

void GameManager::handleInput() {
    // Vérifier si la touche Echap est pressée pour mettre en pause le jeu
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        pause = true;
    }
    else {
        // Si la touche Echap n'est pas pressée, gérer les entrées du joueur pour le jeu
        player.handleInput();
    }
}

void GameManager::update() {
    if (pause) {
        // Si le jeu est en pause, gérer les entrées et la mise à jour du menu de pause
        m_pauseMenu.handleInput();
        m_pauseMenu.update();
        return;
    }
    // Calculer le temps écoulé depuis la dernière mise à jour
    float deltaTime = frameClock.restart().asSeconds();

    // Mettre à jour le bouclier, le joueur et les envahisseurs
    shield.update();
    player.update(deltaTime);
    if (invasionClock.getElapsedTime().asSeconds() > invasionInterval) {
        spawnInvaders();
        invasionClock.restart(); // Réinitialiser l'horloge d'invasion
    }
    for (auto& invader : invaders) {
        invader->update(deltaTime);
    }

    // Mettre à jour les projectiles
    projectiles.update(window, deltaTime);

    // Gérer les collisions entre les projectiles et les envahisseurs, le joueur et le bouclier
    // Augmenter le score si une collision est détectée
    // Gérer également les collisions entre les projectiles et le joueur, et les projectiles et les projectiles
    for (auto it = projectiles.getBullets().begin(); it != projectiles.getBullets().end(); ) {
        for (auto itInvader = invaders.begin(); itInvader != invaders.end(); ) {
            if (it->getGlobalBounds().intersects((*itInvader)->getGlobalBounds())) {
                // Collision entre un projectile et un envahisseur
                (*itInvader)->destroy();
                itInvader = invaders.erase(itInvader);
                it = projectiles.getBullets().erase(it);
                ++counterPoints;
                return;
            }
            else {
                ++itInvader;
            }
        }
        ++it;
    }
    // Gérer les collisions entre les lasers des envahisseurs et le joueur
    for (auto it = projectiles.getLazers().begin(); it != projectiles.getLazers().end(); ) {
        if (it->getGlobalBounds().intersects(player.getGlobalBounds())) {
            // Collision entre le joueur et un laser d'envahisseur
            window.close();
            return;
        }
        ++it;
    }
    // Gérer les collisions entre les lasers et les balles
    for (auto itLazer = projectiles.getLazers().begin(); itLazer != projectiles.getLazers().end(); ) {
        for (auto itBullet = projectiles.getBullets().begin(); itBullet != projectiles.getBullets().end(); ) {
            if (itLazer->getGlobalBounds().intersects(itBullet->getGlobalBounds())) {
                itLazer = projectiles.getLazers().erase(itLazer);
                itBullet = projectiles.getBullets().erase(itBullet);
                return;
            }
            else {
                ++itBullet;
            }
        }
        ++itLazer;
    }
    // Gérer les collisions entre les lasers et le bouclier
    for (auto it = projectiles.getLazers().begin(); it != projectiles.getLazers().end(); ) {
        if (shield.isActivated() && it->getGlobalBounds().intersects(shield.getGlobalBounds())) {
            // Gérer la logique de collision avec le bouclier
            it = projectiles.getLazers().erase(it);
            return;
        }
        ++it;
    }
}

void GameManager::draw() {
    if (pause) {
        // Si le jeu est en pause, dessiner le menu de pause
        m_pauseMenu.draw();
        return;
    }
    // Effacer la fenêtre
    window.clear();
    // Dessiner le joueur, le bouclier et les envahisseurs
    player.draw();
    shield.draw(window);
    for (auto it = invaders.begin(); it != invaders.end(); ++it) {
        if (!(*it)->isDestroyed()) {
            (*it)->draw();
        }
    }
    // Dessiner le score et l'état du bouclier
    scoreText.setString("Score: " + std::to_string(counterPoints));
    window.draw(scoreText);
    if (!shield.isActivated() && !shield.isOnCooldownPeriod()) {
        chargeShield.setString("Shield_State: Ready");
    }
    else {
        chargeShield.setString("Shield_State: In charge");
    }
    window.draw(chargeShield);
    // Afficher le contenu dessiné sur la fenêtre
    window.display();
}

void GameManager::initScoreText() {
    // Charger la police de caractères
    if (!font.loadFromFile("./ui/Faster Stroker.otf")) {
        std::cerr << "Erreur : Impossible de charger la police de caractères." << std::endl;
        return;
    }
    // Initialiser le texte du score
    scoreText.setFont(font);
    scoreText.setCharacterSize(20);
    scoreText.setFillColor(sf::Color::White);
    scoreText.setPosition(10.0f, 10.0f);
}

void GameManager::initShieldCharge() {
    // Charger la police de caractères
    if (!font.loadFromFile("./ui/Faster Stroker.otf")) {
        std::cerr << "Erreur : Impossible de charger la police de caractères." << std::endl;
        return;
    }
    // Initialiser le texte de l'état du bouclier
    chargeShield.setFont(font);
    chargeShield.setCharacterSize(20);
    chargeShield.setFillColor(sf::Color::White);
    chargeShield.setPosition(10.0f, window.getSize().y - 30.0f);
}

void GameManager::spawnInvaders() {
    // Générer les envahisseurs et les placer sur la fenêtre
    for (int i = 0; i < 5; ++i) {
        Invader* invader = new Invader(window, projectiles);
        float xPos = static_cast<float>(i * 150);  // Ajuster la position horizontale
        float yPos = 50.0f;  // Ajuster la position verticale
        invader->getSprite().setPosition(xPos, yPos);
        invaders.push_back(invader);
    }
}

void GameManager::setPause(bool isPaused) {
    // Mettre le jeu en pause ou reprendre le jeu
    pause = isPaused;
}
