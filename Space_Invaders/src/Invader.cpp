#include "Invader.h"
#include <iostream>

// Constructeur de l'envahisseur
Invader::Invader(sf::RenderWindow& window, Projectile& lazers) : window(window), projectiles(lazers) {
    // Charger la texture de l'envahisseur depuis un fichier image
    if (!texture.loadFromFile("./images/invader.png")) {
        std::cout << "Erreur : Impossible de charger l'image de l'envahisseur." << std::endl;
        window.close(); // Fermer la fenêtre en cas d'échec de chargement de la texture
    }

    // Configurer la sprite de l'envahisseur
    invader.setTexture(texture);
    float scaleFactor = 0.3f;
    invader.setScale(scaleFactor, scaleFactor);
    // Positionner l'envahisseur au milieu de la fenêtre en bas
    invader.setPosition(window.getSize().x / 2 - invader.getGlobalBounds().width / 2, window.getSize().y - invader.getGlobalBounds().height - 750);

    // Initialiser les paramètres de mouvement de l'envahisseur
    maxSpeedX = 60.0f;
    direction = 1.0f;
    speedY = 0.0f;
    deadPoint = 1.0f;
    speedX = maxSpeedX * direction * deadPoint;
    onLimit = false;
}

// Méthode de mise à jour de l'envahisseur
void Invader::update(float deltaTime) {
    // Déplacer l'envahisseur horizontalement
    float speedX = maxSpeedX * direction;
    invader.move(speedX * deltaTime, speedY * deltaTime);

    // Gérer le mouvement de l'envahisseur lorsqu'il atteint les limites de l'écran
    if ((invader.getPosition().x < 0 || invader.getPosition().x + invader.getGlobalBounds().width > window.getSize().x) && onLimit == false) {
        deadPoint = 0.0f;
        direction = -direction;
        if (speedY == 0.0f) {
            speedY = 100.0f;
            downClock.restart();  // Redémarrer le chronomètre au début du mouvement vers le bas
        }
        onLimit = true;
    }

    // Gérer le mouvement de l'envahisseur lorsqu'il descend vers le bas
    if (speedY > 0.0f && downClock.getElapsedTime().asSeconds() > 0.8 && downClock.getElapsedTime().asSeconds() < 0.9) {
        speedY = 0.0f;
        deadPoint = 1.0f;
        onLimit = false;
    }

    // Gérer la défaite si l'envahisseur atteint le bas de l'écran
    if (invader.getPosition().y + invader.getGlobalBounds().height > window.getSize().y && invader.getPosition().y + invader.getGlobalBounds().height < window.getSize().y + 10) {
        std::cout << "GAME OVER CHIEF" << std::endl;
        window.close();
    }

    // Gérer le tir de l'envahisseur
    if (shotClock.getElapsedTime().asSeconds() > 0.8) {
        // Effectue un tir
        shoot();
        // Réinitialise le chronomètre
        shotClock.restart();
    }
}

// Méthode de tir de l'envahisseur
void Invader::shoot() {
    // Faire tirer un projectile depuis la position de l'envahisseur
    projectiles.fire(invader.getPosition());
}

// Méthode pour marquer l'envahisseur comme détruit
void Invader::destroy() {
    destroyed = true;
}

// Méthode pour vérifier si l'envahisseur est détruit
bool Invader::isDestroyed() {
    return destroyed;
}

// Méthode pour dessiner l'envahisseur sur la fenêtre
void Invader::draw() const {
    window.draw(invader);
    projectiles.draw(window); // Dessiner les projectiles associés à l'envahisseur
}

// Méthode pour obtenir la sprite de l'envahisseur
sf::Sprite& Invader::getSprite() {
    return invader;
}

// Méthode pour obtenir les limites globales de l'envahisseur
const sf::FloatRect Invader::getGlobalBounds() const {
    return invader.getGlobalBounds();
}
