#include "Menu.h"
#include <SFML/Graphics.hpp>
#include <iostream>

// Constructeur de la classe Menu
Menu::Menu(sf::RenderWindow& window)
    : m_window(window), m_gameStart(false) {
    // Charger la police de caractères depuis un fichier
    if (!m_font.loadFromFile("./ui/Faster Stroker.otf")) {
        std::cerr << "Erreur : Impossible de charger la police de caractères." << std::endl;
        return;
    }

    // Configurer le titre du menu
    m_titleText.setFont(m_font);
    m_titleText.setString("Space Invaders");
    m_titleText.setCharacterSize(48);
    m_titleText.setFillColor(sf::Color::White);
    m_titleText.setStyle(sf::Text::Bold);
    m_titleText.setPosition(260.f, 100.f);

    // Configurer le bouton de démarrage du jeu
    m_startButton.setFont(m_font);
    m_startButton.setString("Start Game");
    m_startButton.setCharacterSize(24);
    m_startButton.setFillColor(sf::Color::White);
    m_startButton.setPosition(400.f, 300.f);

    // Configurer le bouton de quitter
    m_quitButton.setFont(m_font);
    m_quitButton.setString("Quit");
    m_quitButton.setCharacterSize(24);
    m_quitButton.setFillColor(sf::Color::White);
    m_quitButton.setPosition(450.f, 400.f);
}

// Méthode pour gérer les entrées de l'utilisateur
void Menu::handleInput() {
    sf::Event event;
    while (m_window.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_window.close(); // Fermer la fenêtre si la croix de fermeture est cliquée
        }
        else if (event.type == sf::Event::MouseButtonPressed) {
            // Vérifier si les boutons de démarrage du jeu ou de quitter ont été cliqués
            if (event.mouseButton.button == sf::Mouse::Left) {
                sf::Vector2i mousePos = sf::Mouse::getPosition(m_window);

                if (m_startButton.getGlobalBounds().contains(mousePos.x, mousePos.y)) {
                    m_gameStart = true; // Démarrer le jeu
                }
                else if (m_quitButton.getGlobalBounds().contains(mousePos.x, mousePos.y)) {
                    m_window.close(); // Fermer la fenêtre
                }
            }
        }
        else if (event.type == sf::Event::MouseMoved) {
            // Si la souris se déplace, vérifie si elle survole les boutons
            sf::Vector2i mousePos = sf::Mouse::getPosition(m_window);

            if (m_startButton.getGlobalBounds().contains(mousePos.x, mousePos.y)) {
                // Si la souris survole le bouton "Start Game", change sa couleur
                m_startButton.setFillColor(sf::Color::Red); // Couleur à définir selon vos préférences
            }
            else {
                m_startButton.setFillColor(sf::Color::White); // Remettre la couleur normale si la souris ne survole pas le bouton
            }

            if (m_quitButton.getGlobalBounds().contains(mousePos.x, mousePos.y)) {
                // Si la souris survole le bouton "Quit Game", change sa couleur
                m_quitButton.setFillColor(sf::Color::Red); // Couleur à définir selon vos préférences
            }
            else {
                m_quitButton.setFillColor(sf::Color::White); // Remettre la couleur normale si la souris ne survole pas le bouton
            }
        }
    }
}

// Méthode pour mettre à jour le menu (aucun état à mettre à jour dans ce menu simple)
void Menu::update() {
    // Il n'y a pas d'état à mettre à jour dans ce menu simple
}

// Méthode pour dessiner le menu sur la fenêtre
void Menu::draw() {
    m_window.clear(sf::Color::Black);

    // Dessiner le titre et les boutons
    m_window.draw(m_titleText);
    m_window.draw(m_startButton);
    m_window.draw(m_quitButton);

    m_window.display(); // Afficher le contenu dessiné sur la fenêtre
}

// Méthode pour savoir si le jeu a démarré
bool Menu::isGameStart() const {
    return m_gameStart;
}
