#include "Shield.h"

// Constructeur de la classe Shield
Shield::Shield() : shield(sf::Vector2f(50.0f, 10.0f)), isActive(false), isOnCooldown(false),
                   shieldDuration(3.0f), cooldownDuration(5.0f) {
    shield.setFillColor(sf::Color(255, 182, 193, 150)); // Définition de la couleur du bouclier (rose semi-transparent)
}

// Méthode pour activer le bouclier à la position actuelle du joueur
void Shield::protectPlayer(sf::Vector2f playerPosition) {
    if (!isActive && !isOnCooldown) { // Vérification que le bouclier n'est ni activé ni en cours de refroidissement
        shield.setPosition(playerPosition.x - 5, playerPosition.y - shield.getSize().y - 15); // Positionnement du bouclier au-dessus du joueur
        activationClock.restart(); // Redémarrage de l'horloge pour suivre la durée d'activation du bouclier
        isActive = true; // Activation du bouclier
    }
}

// Méthode pour mettre à jour l'état du bouclier
void Shield::update() {
    if (isActive) {
        if (activationClock.getElapsedTime().asSeconds() > shieldDuration) { // Vérification de la durée d'activation
            isActive = false; // Désactivation du bouclier
            isOnCooldown = true; // Activation du refroidissement
            cooldownClock.restart(); // Redémarrage de l'horloge pour suivre la durée de refroidissement
        }
    }

    if (isOnCooldown) {
        if (cooldownClock.getElapsedTime().asSeconds() > cooldownDuration) { // Vérification de la durée de refroidissement
            isOnCooldown = false; // Fin du refroidissement
        }
    }
}

// Méthode pour dessiner le bouclier sur la fenêtre
void Shield::draw(sf::RenderWindow& window) {
    if (isActive) {
        window.draw(shield); // Dessin du bouclier s'il est activé
    }
}

// Méthode pour vérifier si le bouclier est activé
bool Shield::isActivated() const {
    return isActive;
}

// Méthode pour vérifier si le bouclier est en période de refroidissement
bool Shield::isOnCooldownPeriod() const {
    return isOnCooldown;
}
