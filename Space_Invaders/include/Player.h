#pragma once

// Inclusion des en-têtes nécessaires
#include <SFML/Graphics.hpp>
#include "Projectile.h"
#include "Shield.h"

// Déclaration de la classe Player
class Player {
public:
    // Constructeur prenant une référence vers la fenêtre SFML, les projectiles et le bouclier
    Player(sf::RenderWindow& window, Projectile& projectiles, Shield& shield);

    // Méthode pour gérer les entrées utilisateur
    void handleInput();

    // Méthode pour dessiner le joueur
    void draw();

    // Méthode pour activer le bouclier
    void protect();

    // Méthode pour mettre à jour l'état du joueur
    void update(float deltaTime);

    // Méthode pour obtenir les limites globales du joueur
    const sf::FloatRect getGlobalBounds() const {
        return player.getGlobalBounds();
    }

private:
    // Référence vers la fenêtre SFML
    sf::RenderWindow& window;

    // Texture et sprite représentant le joueur
    sf::Texture texture;
    sf::Sprite player;

    // Horloges pour gérer le temps écoulé depuis le dernier tir et la dernière mise à jour
    sf::Time lastShootTime;
    sf::Clock updateClock;
    sf::Clock shieldClock;

    // Références vers les projectiles et le bouclier
    Projectile& projectiles;
    Shield& shield;

    // Temps de rechargement entre les tirs
    float reload_time;

    // Méthode privée pour effectuer un tir
    void shoot();
};
