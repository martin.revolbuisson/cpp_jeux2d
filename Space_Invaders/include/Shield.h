#pragma once

// Inclusion des en-têtes nécessaires
#include <SFML/Graphics.hpp>

// Déclaration de la classe Shield
class Shield {
private:
    // Attributs de la classe
    sf::RectangleShape shield; // Représente la forme du bouclier
    sf::Clock activationClock; // Chronomètre pour suivre la durée d'activation du bouclier
    sf::Clock cooldownClock;   // Chronomètre pour suivre la durée de refroidissement du bouclier
    bool isActive;             // Indique si le bouclier est activé ou non
    bool isOnCooldown;         // Indique si le bouclier est en période de refroidissement
    const float shieldDuration;    // Durée pendant laquelle le bouclier reste activé
    const float cooldownDuration;  // Durée du refroidissement après que le bouclier a été désactivé

public:
    // Constructeur de la classe
    Shield();

    // Méthode pour protéger le joueur avec le bouclier à une position donnée
    void protectPlayer(sf::Vector2f playerPosition);

    // Méthode pour mettre à jour l'état du bouclier
    void update();

    // Méthode pour dessiner le bouclier sur la fenêtre
    void draw(sf::RenderWindow& window);

    // Méthodes pour vérifier si le bouclier est activé ou en période de refroidissement
    bool isActivated() const;
    bool isOnCooldownPeriod() const;

    // Méthode pour obtenir les limites globales du bouclier
    const sf::FloatRect getGlobalBounds() const {
        return shield.getGlobalBounds();
    }
};
