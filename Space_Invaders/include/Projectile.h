#pragma once

// Inclusion des en-têtes nécessaires
#include <SFML/Graphics.hpp>

// Déclaration de la classe Projectile
class Projectile {
public:
    // Méthode pour mettre à jour les projectiles en fonction du temps écoulé
    void update(sf::RenderWindow& window, float deltaTime);

    // Méthode pour dessiner les projectiles sur la fenêtre
    void draw(sf::RenderWindow& window) const;

    // Méthodes pour obtenir une référence vers les vecteurs de balles et de lasers
    std::vector<sf::RectangleShape>& getBullets();
    std::vector<sf::RectangleShape>& getLazers();

    // Méthodes pour tirer des balles et des lasers depuis une position donnée
    void shoot(sf::Vector2f playerPosition);
    void fire(sf::Vector2f playerPosition);

private:
    // Vecteurs stockant les balles et les lasers
    std::vector<sf::RectangleShape> bullets;
    std::vector<sf::RectangleShape> lazers;
};
