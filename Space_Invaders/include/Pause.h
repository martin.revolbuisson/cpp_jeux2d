#ifndef PAUSE_H
#define PAUSE_H

// Inclusion des en-têtes nécessaires
#include <SFML/Graphics.hpp>

// Déclaration de la classe Pause
class Pause {
public:
    // Constructeur prenant une référence vers la fenêtre SFML
    Pause(sf::RenderWindow& window);

    // Méthode pour gérer les entrées utilisateur
    void handleInput();

    // Méthode pour mettre à jour l'état du menu de pause (non utilisée dans cet exemple)
    void update();

    // Méthode pour dessiner le menu de pause
    void draw();

    // Méthode pour vérifier si le jeu a repris
    bool isResumed() const;

    // Méthode pour vérifier si le jeu a été quitté depuis le menu de pause
    bool isQuit() const;

private:
    // Référence vers la fenêtre SFML
    sf::RenderWindow& m_window;

    // Police et textes utilisés pour afficher le menu de pause
    sf::Font m_font;
    sf::Text m_titleText;
    sf::Text m_resumeButton;
    sf::Text m_quitButton;

    // Booléens indiquant si le jeu a repris ou a été quitté depuis le menu de pause
    bool m_resumed;
    bool m_quit;
};

#endif // PAUSE_H
