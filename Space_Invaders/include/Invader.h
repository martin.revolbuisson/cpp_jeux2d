#pragma once

// Inclusion des en-têtes nécessaires
#include <SFML/Graphics.hpp>
#include "Projectile.h"

// Déclaration de la classe Invader
class Invader {
public:
    // Constructeur prenant une référence vers la fenêtre SFML et une référence vers les projectiles
    Invader(sf::RenderWindow& window, Projectile& lazers);

    // Méthode pour mettre à jour l'invader en fonction du temps écoulé depuis la dernière mise à jour
    void update(float deltaTime);

    // Méthode pour marquer l'invader comme détruit
    void destroy();

    // Méthode pour faire tirer l'invader
    void shoot();

    // Méthode pour vérifier si l'invader est détruit
    bool isDestroyed();

    // Méthode pour dessiner l'invader
    void draw() const;

    // Méthode pour obtenir la référence vers le sprite de l'invader
    sf::Sprite& getSprite();

    // Méthode pour obtenir les limites globales de l'invader
    const sf::FloatRect getGlobalBounds() const;

private:
    // Référence vers la fenêtre SFML
    sf::RenderWindow& window;

    // Texture et sprite de l'invader
    sf::Texture texture;
    sf::Sprite invader;

    // Booléen indiquant si l'invader est détruit
    bool destroyed = false;

    // Vitesses horizontale et verticale de l'invader
    float speedX;
    float speedY;

    // Point mort et indicateur de bord pour le mouvement
    float deadPoint;
    bool onLimit;

    // Vitesse maximale horizontale et direction de l'invader
    float maxSpeedX;
    float direction;

    // Horloges pour le tir et le mouvement vers le bas
    sf::Clock shotClock;
    sf::Clock downClock;

    // Référence vers les projectiles
    Projectile& projectiles;
};
