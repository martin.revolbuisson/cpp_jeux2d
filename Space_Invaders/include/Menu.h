#pragma once
#ifndef MENU_H
#define MENU_H

// Inclusion des en-têtes nécessaires
#include <SFML/Graphics.hpp>

// Déclaration de la classe Menu
class Menu {
public:
    // Constructeur prenant une référence vers la fenêtre SFML
    Menu(sf::RenderWindow& window);

    // Méthode pour gérer les entrées utilisateur
    void handleInput();

    // Méthode pour mettre à jour l'état du menu (non utilisée dans cet exemple)
    void update();

    // Méthode pour dessiner le menu
    void draw();

    // Méthode pour vérifier si le jeu a démarré
    bool isGameStart() const;

private:
    // Référence vers la fenêtre SFML
    sf::RenderWindow& m_window;

    // Police et textes utilisés pour afficher le menu
    sf::Font m_font;
    sf::Text m_titleText;
    sf::Text m_startButton;
    sf::Text m_quitButton;

    // Booléen indiquant si le jeu a démarré
    bool m_gameStart;
};

#endif // MENU_H
