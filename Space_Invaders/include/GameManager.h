#pragma once

// Inclusion des fichiers d'en-tête nécessaires
#include "Invader.h"
#include "Player.h"
#include "Projectile.h"
#include "Shield.h"
#include "Pause.h"
#include <vector>

// Déclaration de la classe GameManager
class GameManager {
public:
    // Constructeur prenant une référence vers la fenêtre SFML
    GameManager(sf::RenderWindow& window);

    // Destructeur
    ~GameManager();

    // Méthodes pour gérer les entrées, mettre à jour et dessiner le jeu
    void handleInput();
    void update();
    void draw();

    // Méthode pour générer les envahisseurs
    void spawnInvaders();

    // Méthodes pour initialiser le texte du score et le chargement du bouclier
    void initScoreText();
    void initShieldCharge();

    // Méthode pour définir l'état de pause du jeu
    void setPause(bool isPaused);

private:
    // Référence vers la fenêtre SFML
    sf::RenderWindow& window;

    // Objets du jeu
    Projectile projectiles;
    Player player;
    Shield shield;

    // Score du joueur
    int counterPoints;

    // Vecteur d'invaders pour gérer plusieurs envahisseurs
    std::vector<Invader*> invaders;

    // Horloges pour gérer le temps et les intervalles d'invasion
    sf::Clock invasionClock;
    sf::Clock frameClock;
    float invasionInterval;

    // Variable pour indiquer si le jeu est en pause
    bool pause;

    // Police et textes pour le score et l'état du bouclier
    sf::Font font;
    sf::Text scoreText;
    sf::Text chargeShield;

    // Menu de pause
    Pause m_pauseMenu;
};
