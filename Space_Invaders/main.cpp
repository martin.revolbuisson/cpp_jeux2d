#include <SFML/Graphics.hpp>
#include "GameManager.h"
#include "Menu.h"

int main() {
    // Créer une fenêtre SFML de taille 1024x800 pixels avec le titre "Simple 2D Game"
    sf::RenderWindow window(sf::VideoMode(1024, 800), "Simple 2D Game");
    // Limiter le nombre d'images par seconde (FPS) à 60
    window.setFramerateLimit(60);

    // Créer une instance de la classe Menu avec la fenêtre SFML comme argument
    Menu menu(window);
    
    // Boucle principale pour afficher le menu et attendre que le jeu démarre
    while (window.isOpen() && !menu.isGameStart()) {
        // Gérer les entrées de l'utilisateur pour le menu
        menu.handleInput();
        // Mettre à jour l'état du menu
        menu.update();
        // Dessiner le menu sur la fenêtre
        menu.draw();
    }

    // Si la fenêtre est toujours ouverte après avoir quitté le menu, démarrer le jeu
    if (window.isOpen()) {
        // Créer une instance de la classe GameManager avec la fenêtre SFML comme argument
        GameManager gameManager(window);

        // Boucle principale du jeu
        while (window.isOpen()) {
            // Gérer les événements de la fenêtre
            sf::Event event;
            while (window.pollEvent(event)) {
                // Si l'utilisateur ferme la fenêtre, la fermer également
                if (event.type == sf::Event::Closed) {
                    window.close();
                }
            }

            // Gérer les entrées de l'utilisateur pour le jeu
            gameManager.handleInput();
            // Mettre à jour l'état du jeu
            gameManager.update();
            // Dessiner le jeu sur la fenêtre
            gameManager.draw();
        }
    }

    return 0;
}
